---
title: GraphQL Queries (Deprecated)
layout: ../../layouts/MainLayout.astro
---

**Drupal State GraphQL queries are Deprecated**

The GraphQL Queries option was experimental, and while it was a nice feature, it
proved difficult to maintain a store with potentially two different shapes.
