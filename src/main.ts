import { DrupalJsonApiParams } from 'drupal-jsonapi-params';
import DrupalState from './DrupalState';
import './style.css';

const app = document.querySelector<HTMLDivElement>('#app')!;

const store: DrupalState = new DrupalState({
  apiBase: 'https://dev-ds-demo.pantheonsite.io/',
  apiPrefix: 'jsonapi',
  defaultLocale: 'en',
  debug: true,
  // Example custom error handler
  // onError: err => {
  //   // if there is a status code it will be at the end of the error message
  //   const [statusCode] = err?.message?.match(/([0-5]{3})$/gm) || [null];
  //   let renderErr;
  //   if (statusCode === '404') {
  //     renderErr = `<h2>${err.message}</h2>`;
  //   } else if (statusCode === '500') {
  //     renderErr = `<h2>Something went wrong. Please try again later!</h2>`;
  //   } else {
  //     renderErr = `<pre>${JSON.stringify(err.message ? err.message : err, null, 2)}<pre>`;
  //   }
  //   // set error and statusCode to global state
  //   store.setState({ error: err, statusCode: statusCode });
  //   app.innerHTML += renderErr + '<br>';
  //   return;
  // },
});

const withoutStore: DrupalState = new DrupalState({
  apiBase: 'https://dev-ds-demo.pantheonsite.io/',
  apiPrefix: 'jsonapi',
  defaultLocale: 'en',
  debug: true,
  noStore: true,
});

// Uncomment to use authenticated store - currently depends on local environment
// const authStore: any = new DrupalState({
//   apiBase: 'https://demo-decoupled-bridge.lndo.site',
//   apiPrefix: 'jsonapi',
//   defaultLocale: 'en',
//   clientId: 'my-client-id',
//   clientSecret: 'mysecret',
//   debug: true,
//   onError: (err: Error) => {
//     console.log("An exception was caught!!!")
//     console.error(err)
//   }
// });

async function main(): Promise<void> {
  // You can use DrupalJsonApiParams to construct JSON:API query string parameters...
  // see https://www.npmjs.com/package/drupal-jsonapi-params for documentation
  const jsonApiParams = new DrupalJsonApiParams();
  jsonApiParams.addInclude(['field_media_image.field_media_image']);

  // ...or use a query string of your own construction.
  // see https://www.drupal.org/docs/core-modules-and-themes/core-modules/jsonapi-module/fetching-resources-get for more information
  const myQueryStringParams = 'include=field_media_image.field_media_image';

  console.log(
    '--- If no resources are in state, create a new resource object ---'
  );
  console.log(
    await store.getObject({
      objectName: 'node--recipe',
      id: '33386d32-a87c-44b9-b66b-3dd0bfc38dca',
      params: jsonApiParams,
    })
  );
  console.log(
    '--- If a resource is in state, use the local version rather than fetching ---'
  );
  console.log(
    await store.getObject({
      objectName: 'node--recipe',
      id: '33386d32-a87c-44b9-b66b-3dd0bfc38dca',
    })
  );

  console.log('--- Fetch and add to the existing resource object ---');
  console.log(
    await store.getObject({
      objectName: 'node--recipe',
      id: '50c3e7c9-64a9-453c-9289-278132beb4a2',
      params: myQueryStringParams,
    })
  );
  console.log(
    '--- If a resource is in state, use the local version rather than fetching ---'
  );
  console.log(
    await store.getObject({
      objectName: 'node--recipe',
      id: '50c3e7c9-64a9-453c-9289-278132beb4a2',
    })
  );

  console.log('--- Fetch a collection ---');
  console.log(
    await store.getObject({
      objectName: 'node--recipe',
      params: myQueryStringParams,
    })
  );

  console.log('--- Get collection from state if it already exists ---');
  console.log(
    await store.getObject({
      objectName: 'node--recipe',
    })
  );
  console.log('--- If a resource exists in collection state, use that ---');
  console.log(
    await store.getObject({
      objectName: 'node--recipe',
      id: '21a95a3d-4a83-494f-b7b4-dcfb0f164a74',
    })
  );

  // gets the first 50 results only
  console.log('--- Fetch objects of a type ---');
  console.log(
    await store.getObject({
      objectName: 'node--ds_example',
    })
  );
  // get the remaining results
  console.log('--- Fetch _all_ objects of a type ---');
  console.log(
    await store.getObject({
      objectName: 'node--ds_example',
      all: true,
    })
  );

  console.log('--- Fetch _all_ objects of a type with params---');
  console.log(
    await store.getObject({
      objectName: 'node--ds_example',
      all: true,
      params: 'fields[node--ds_example]=title,body',
    })
  );

  console.log('-- Ignore the store and force fetch data from Drupal --');
  console.log(
    await store.getObject({
      objectName: 'node--recipe',
      id: '50c3e7c9-64a9-453c-9289-278132beb4a2',
      refresh: true,
      params: jsonApiParams,
    })
  );

  // It is possible to fetch menu data using the jsonapi_menu_items module along with jsonapi_hypermedia
  // jsonapi_hypermedia module: https://www.drupal.org/project/jsonapi_hypermedia
  // jsonapi_menu_items module: https://www.drupal.org/project/jsonapi_menu_items
  console.log('--- Fetch Menu items---');
  console.log(await store.getObject({ objectName: 'menu_items--main' }));

  console.log(store.getState());
  // Uncomment to use authenticated store - currently depends on local environment

  // console.log(
  //   '--- Fetch an object by path where path needs to be translated ---'
  // );
  // console.log(
  //   await authStore.getObjectByPath({
  //     objectName: 'node--recipe',
  //     path: '/recipes/fiery-chili-sauce',
  //   })
  // );
  // console.log('--- Add a second path to dsPathTranslations in state ---');
  // console.log(
  //   await authStore.getObjectByPath({
  //     objectName: 'node--recipe',
  //     path: '/recipes/victoria-sponge-cake',
  //   })
  // );
  // console.log('--- Use path translation in state if it already exists ---');
  // console.log(
  //   await authStore.getObjectByPath({
  //     objectName: 'node--recipe',
  //     path: '/recipes/victoria-sponge-cake',
  //   })
  // );

  // console.log('--- Get taxonomy with authentication ---');
  // console.log(
  //   await authStore.getObject({
  //     objectName: 'taxonomy_vocabulary--taxonomy_vocabulary',
  //   })
  // );

  // console.log('-- Fetch an object anonymously --');
  // console.log(
  //   await authStore.getObject({
  //     objectName: 'node-recipe',
  //     anon: true,
  //   })
  // );

  // console.log('-- Also works with getObjectByPath --');
  // console.log(
  //   await authStore.getObjectByPath({
  //     objectName: 'node-recipe',
  //     path: '/recipes/fiery-chili-sauce',
  //     anon: true,
  //   })
  // );
  // You also have direct access to the Zustand store if necessary
  store.setState({ custom: 'custom state' });

  console.log('--- Fetch version of resource - without store ---');
  console.log(
    await withoutStore.getObject({
      objectName: 'node--recipe',
      id: '33386d32-a87c-44b9-b66b-3dd0bfc38dca',
    })
  );

  console.log('--- Fetch _all_ objects of a type - without store ---');
  console.log(
    await withoutStore.getObject({
      objectName: 'node--ds_example',
      all: true,
    })
  );

  console.log(
    '--- Fetch an object by path where path needs to be translated - without store ---'
  );
  console.log(
    await withoutStore.getObjectByPath({
      objectName: 'node--recipe',
      path: '/recipes/deep-mediterranean-quiche',
    })
  );
}

await main();

app.innerHTML = `<pre>${JSON.stringify(store.getState(), null, 2)}</pre>`;
