import { defineConfig } from 'tsup';
import pkgJson from './package.json';

export default defineConfig({
  entry: [
    './src/**/*.ts',
    './lib.ts',
    '!./src/**/__tests__/*',
    '!./src/(main.ts|vite-env.d.ts)',
  ],
  splitting: false,
  treeshake: true,
  dts: true,
  clean: true,
  skipNodeModulesBundle: true,
  outDir: './dist',
  format: ['esm', 'cjs'],
  external: [...Object.keys(pkgJson.devDependencies)],
  minify: true,
  platform: 'neutral',
});
