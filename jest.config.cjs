module.exports = {
  roots: ['<rootDir>/src'],
  testMatch: [
    '**/__tests__/**/*.+(ts|tsx|js)',
    '**/?(*.)+(spec|test).+(ts|tsx|js)',
    '!**/__tests__/**/*.json.ts',
  ],
  /**
   * This is a workaround for node-fetch v3 and jest ESM compatibility
   * using node-fetch v2 for tests since v3 is ESM only and breaks our tests.
   * see https://github.com/node-fetch/node-fetch/discussions/1503#discussioncomment-4843507
   */
  moduleNameMapper: {
    'node-fetch': '<rootDir>/node_modules/node-fetch-jest',
  },
  transform: {
    '^.+\\.(ts|tsx)$': 'ts-jest',
  },
  reporters: ['default', 'jest-junit'],
};
